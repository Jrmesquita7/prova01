package banco;

public class Aplicativo {

	public static void main(String[] args) {
		Carteira cartpf = new Carteira("CarteiraPF");
		Carteira cartpj = new Carteira("CarteiraPJ");
		
		PessoaFisica pf = new PessoaFisica("Cleobeto", "12345");
		PessoaJuridica pj = new PessoaJuridica("Cleobeto", "54321","PauloSA");
		
		if (cartpf.abrirConta(pf)) {
			System.out.println("Conta Pessoa Fisica Criada");
		}
		
		if (cartpj.abrirConta(pj)) {
			System.out.println("Conta Pessoa Juridica Criada");
		}
		
		cartpf.getCarteirapf();
 		cartpj.getCarteirapj();
	}	
}