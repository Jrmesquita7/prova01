package banco;

import java.util.Date;

public class Pessoa {
	private String nome;
	private Date dtanasc;
	private String endereco;
	private String senha;
	
	public Pessoa(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Date getDtaNasc() {
		return dtanasc;
	}
	
	public String getEndereco() {
		return endereco;
	}
	
	public String getSenha() {
		return senha;
	}

}	

	

