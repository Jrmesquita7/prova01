package banco;

public class PessoaJuridica extends Pessoa {
	
	private String cnpj;
	private String nomeFantasia;
		
	public PessoaJuridica(String nome, String cnpj, String nomeFantasia) {
		super(nome);
		this.cnpj = cnpj;
		this.setNomeFantasia(nomeFantasia);
	}	

	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

}