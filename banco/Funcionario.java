package banco;

public class Funcionario extends PessoaFisica {
	
	private Double salario;
	
	public Funcionario(String nome, Double salario) {
		super(nome);
		this.setSalario(salario);
		
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}
}